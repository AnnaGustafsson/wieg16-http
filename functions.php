<?php
// "HTTP/1.0 404 Not Found"
function status_header($code = 200)
{
    $messages = [
        200 => "Okidoki",
        300 => "Multiple Choices",
        301 => "Moved Permanently",
        302 => "Moved Temporarily",
        304 => "Not Modified",
        307 => "Temporary Redirect",
        400 => "Bad Request",
        401 => "Unauthorized",
        403 => "Forbidden",
        404 => "Not Found",
        410 => "Gone",
        500 => "Internal Server Error",
        501 => "Not Implemented",
        503 => "Service Unavailable",
        550 => "Permission denied",
    ];
    header("HTTP/1.0 " . $code . " " . $messages[$code]);
}

status_header();

// [header => värde]
// Connection: keep-alive
function headers(array $headers = []){

    foreach($headers as $header => $value){
        header("$header: $value");
    }
}
headers([
    "Namn"=>"Anna",
    "Utbildning" => "Webbutveckling",
]);


function redirect($url, $code = 302){
    header("Location: " . $url);
}
redirect('http://google.se');